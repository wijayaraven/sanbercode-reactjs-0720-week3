import React from 'react';

// class Welcome extends React.Component {
//   render() {
//     return <h1>Hello, {this.props.name}</h1>;
//   }
// }

// class ShowAge extends React.Component {
//   render() {
//     return <h1>your age is {this.props.age}</h1>;
//   }
// }

// var person = [
//   {name: "sarah", age: 25},
//   {name: "michael", age: 30},
//   {name: "john", age: 33}
// ]

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
  ]

class TabelBuah extends React.Component {
    render() {
        return (
            <>
            <h1 style={{textAlign:"center"}}>Tabel Harga Buah </h1>
                    <table style={{border: "2px solid black",marginLeft: "auto",marginRight: "auto"}}>
                        <tr style={{backgroundColor:"#ccc7c4"}}>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        </tr>
                            {dataHargaBuah.map(el=> {
                            return (
                                <tr style={{backgroundColor:"#f67833"}}>
                                    <td style={{paddingRight:"400px"}}>{el.nama}</td>
                                    <td style={{paddingRight:"100px"}}>{el.harga}</td>
                                    <td style={{paddingRight:"100px"}}>{el.berat}</td>
                                </tr>
                            )})}
                     </table> 


         
                   
                     </>
        )
    }
}

export default TabelBuah