import React, {useContext} from "react"
import {Link} from "react-router-dom";
import {ColorNavContext} from "./ColorNavContext";

const NavColorList = () =>{
  const [color, setColor] = useContext(ColorNavContext);

  return(
    <><div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">

<div class="collapse navbar-collapse" id="navbarSupportedContent">
  <ul style={{textAlign:"center"}}>
          <li style={{ 
              background:color[1], 
            fontweight: "bold",
            display:"inline-block",
            padding: "10px"}}>
            <Link to="/">Home</Link>
          </li>
          <li style={{  
              background:color[0],
            fontweight: "bold",
            display:"inline-block",
            padding: "10px"}}>
            <Link to="/Tugas11">Tugas11</Link>
          </li >
          <li style={{  
              background:color[3],
            fontweight: "bold",
            display:"inline-block",
            padding: "10px"}}>
            <Link to="/Tugas12">Tugas12</Link>
          </li>
          <li style={{  
              background:color[4],
            fontweight: "bold",
            display:"inline-block",
            padding: "10px"}}>
            <Link to="/Tugas13">Tugas13</Link>
          </li>
          <li style={{  
              background:color[5],
            fontweight: "bold",
            display:"inline-block",
            padding: "10px"}}>
            <Link to="/Tugas14">Tugas14</Link>
          </li>
          <li style={{  
              background:color[6],
            fontweight: "bold",
            display:"inline-block",
            padding: "10px"}}>
            <Link to="/Tugas15">Tugas15</Link>
          </li>
  </ul>

</div>
</nav>
</div>

     
    </>
  )

}

export default NavColorList
