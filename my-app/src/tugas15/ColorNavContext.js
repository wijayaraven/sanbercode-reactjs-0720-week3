import React, { useState, createContext } from "react";

export const ColorNavContext = createContext();

export const ColorNavProvider = props => {
  const [color, setColor] =  useState(["#e169b4", "#ca5ea2", "#f6d2e8", "#69e1ae", "#776cf4","#6cf1f4","#a1f46c"])

  return (
    <ColorNavContext.Provider value={[color, setColor]}>
      {props.children}
    </ColorNavContext.Provider>
  );
};
