import React, {useContext} from "react"
import {BuahContext} from "./BuahContext"
import axios from "axios";
import Timer from "../tugas12/Timer";


const BuahList = () =>{
  const [
    daftarBuah, setDaftarBuah,
    inputNamaBuah, setInputNamaBuah,
    inputHargaBuah, setInputHargaBuah,
    inputBeratBuah, setInputBeratBuah,
    indexOfForm, setIndexOfForm,
    id, setId
  ] = useContext(BuahContext);

  const handleEdit = (event) =>{
    let index = event.target.value;
    let id = event.target.id;
    let dataBuah = daftarBuah[index];

    setInputNamaBuah(dataBuah.name)
    setInputHargaBuah(dataBuah.price)
    setInputBeratBuah(dataBuah.weight)
    setIndexOfForm(index)
    setId(id);
  }

  const handleDelete = (event) => {
    let index = event.target.value
    let id = event.target.id;
    let newDaftarBuah = daftarBuah
    newDaftarBuah.splice(index, 1)

    setDaftarBuah([...newDaftarBuah])

    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
  }

  return(
    <>
        <h1 style={{textAlign:"center"}}>Tabel Harga Buah </h1>
                      <table style={{border: "2px solid black",marginLeft: "auto",marginRight: "auto"}}>
                          <tr style={{backgroundColor:"#ccc7c4"}}>
                          <th>no</th>
                          <th>Nama</th>
                          <th>Harga</th>
                          <th>Berat</th>
                          <th>Aksi</th>
                          </tr>

                      {daftarBuah.map((el, index)=>{
                      return(
                              <tr style={{backgroundColor:"#f67833"}}>
                              <td>{index+1}</td>
                                  <td style={{paddingRight:"400px"}}>{el.name}</td>
                                  <td style={{paddingRight:"100px"}}>{el.price}</td>
                                  <td style={{paddingRight:"100px"}}>{el.weight}</td>
                                  <td> 
                                  <button id={el.id} onClick={handleEdit} value={index}> Edit </button>
                                  &nbsp;
                                  <button id={el.id} onClick={handleDelete} value={index}>Delete</button> 
                            </td>
                          </tr>
                          )})}
                     </table> 
          <Timer/>
    </>
  )

}

export default BuahList