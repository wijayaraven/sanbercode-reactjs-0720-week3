import React, {Component} from "react"


class Lists extends Component{

  constructor(props){
    super(props)
    this.state ={
    databuah:[
      {nama: "Semangka", harga: 10000, berat: 1000},
      {nama: "Anggur", harga: 40000, berat: 500},
      {nama: "Strawberry", harga: 30000, berat: 400},
      {nama: "Jeruk", harga: 30000, berat: 1000},
      {nama: "Mangga", harga: 30000, berat: 500}
    ],

     inputNama : "",
     inputHarga : "",
     inputBerat : "",
     indexofForm:-1   
    }

    this.handleChangeNama = this.handleChangeNama.bind(this);
    this.handleChangeHarga = this.handleChangeHarga.bind(this);
    this.handleChangeBerat = this.handleChangeBerat.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleDelete(event){
    let index = event.target.value
    let newdata = this.state.databuah
    let editeddata = newdata[this.state.indexOfForm]
    newdata.splice(index, 1)
  
    if (editeddata !== undefined){
      // array findIndex baru ada di ES6
      var newIndex = newdata.findIndex((el) => el === editeddata)
      this.setState({databuah: newdata, indexOfForm: newIndex})
      
    }else{
      
      this.setState({databuah:newdata})
    }
    
  }

  handleEdit(event){
    let index =event.target.value
    let Nama=this.state.databuah[index].nama
    let Harga=this.state.databuah[index].harga
    let Berat=this.state.databuah[index].berat
    this.setState({inputNama:Nama,
                  inputHarga:Harga,
                  inputBerat:Berat,
                  indexofForm:index})
  }

  handleChangeNama(event){
    this.setState({inputNama: event.target.value});
  }
  handleChangeHarga(event){
    this.setState({inputHarga: event.target.value});
  }
  handleChangeBerat(event){
    this.setState({inputBerat: event.target.value});
  }

  handleSubmit(event){
    event.preventDefault()
  
    let nama=this.state.inputNama
    let berat=this.state.inputBerat
    let harga=this.state.inputHarga
                               
    if (nama.replace(/\s/g,'') !== "" && berat.replace(/\s/g,'') !== "" &&harga.replace(/\s/g,'') !== ""){     
     
      let newdata=this.state.databuah
      let index=this.state.indexofForm
      let data={nama:this.state.inputNama,
                harga:this.state.inputHarga,
                berat:this.state.inputBerat}
               
                                    if (index === -1){
                                      newdata = [...newdata, data]
                                    }else{
                                      newdata[index] = data
                                    }
                                
                                    this.setState({
                                      databuah: newdata,
                                      inputNama : "",
                                      inputHarga : "",
                                      inputBerat : "",
                                      indexofForm: -1
                                    })}
                                  
                              

  
  }

  render(){
    return(
      <>
        <h1 style={{textAlign:"center"}}>Tabel Harga Buah </h1>
                  <table style={{border: "2px solid black",marginLeft: "auto",marginRight: "auto"}}>
                      <tr style={{backgroundColor:"#ccc7c4"}}>
                      <th>Nama</th>
                      <th>Harga</th>
                      <th>Berat</th>
                      <th>Aksi</th>
                      </tr>
                          {this.state.databuah.map((el,index)=> {
                          return (
                              <tr style={{backgroundColor:"#f67833"}}>
                                  <td style={{paddingRight:"400px"}}>{el.nama}</td>
                                  <td style={{paddingRight:"100px"}}>{el.harga}</td>
                                  <td style={{paddingRight:"100px"}}>{el.berat}</td>
                                  <td > 
                                    <button onClick={this.handleEdit} value={index}> EDIT </button>
                                    &nbsp;
                                     <button onClick={this.handleDelete} value={index}>Delete</button>

                                  </td>
                              </tr>
                          )})}
                   </table> 
        {/* Form */}
    
        <h1 style={{textAlign:"center"}}>Form DataBuah</h1>
        <div style={{marginLeft:"350px"}}>
        <table>
        <th style={{paddingRight:"100px"}}>nama</th>
        <th style={{paddingRight:"100px"}}>harga</th>
        <th>berat</th>
        </table>
        
        <form onSubmit={this.handleSubmit}>
          <input type="text" value={this.state.inputNama} onChange={this.handleChangeNama}/>
          <input type="text" value={this.state.inputHarga} onChange={this.handleChangeHarga}/>
          <input type="text" value={this.state.inputBerat} onChange={this.handleChangeBerat}/>
          <button>submit</button>
        </form>


        </div>
      
      </>
    )
  }
}
















export default Lists
